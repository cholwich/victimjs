function randomStudent() {
	prepareProb();

	var students = data.current;
	var N = students.length;
	var student;
	
	var r = Math.random();
	for(var i=0; i<N; i++) {
		if (r <= students[i].p) {
			break;
		}
	}
	student = students[i];
	data.current.splice(i,1);
	
	//console.log(data.current.length);
	if (data.current.length == 0) {
		data.current = data.students.slice(0);
	}
	data.picked = student;

	var display = $('#displayname');
	display.removeClass('blue');
	$('#displayid').html('&nbsp;');

	rep(0, data.students, data.students.length, display, student);
}

function rep(x, students, N, display, student) {
	if (x >= 30) {
		//console.log("Finished!!!");
		var r = Math.random();
		if (r > 0.5) {
			display.text(getName(student));
		}
		else {
			var j = Math.floor(Math.random()*N);
			display.text(getName(students[j]));
		}
		fitDisplay(display);
		setTimeout(showFinal, 1300, display, student);
	}
	else {
		var j = Math.floor(Math.random()*N);
		display.text(getName(students[j]));
		fitDisplay(display);
		setTimeout(rep, 50, x+1, students, N, display, student);
	}
}

function showFinal(display, student) {
	display.text(getName(student));
	display.addClass('blue');
	fitDisplay(display);
	updateDisplay(student);
}

function fitDisplay(display) {
	//display.fitText(1.5, {minFontSize: '32px', maxFontSize:'144px'});
	//display.boxfit();
}

function getName(s) {
	return s.fname + " " +s.lname;
}

function prepareProb() {
	//var students = data.students;
	var students = data.current;
	var N = students.length;

	var sum_weight = 0.0;

	for(var i=0; i<N; i++) {
		students[i].weight = 1 / Math.pow(4, students[i].frequency);
		//console.log(students[i].frequency);
		sum_weight += students[i].weight; 
	}

	//console.log(sum_weight);

	for(var i=0; i<N; i++) {
		students[i].weight /= sum_weight;
	}

	//for(var i=0; i<N; i++) {
	//	console.log(students[i].fname+":"+students[i].weight);
	//}
	
	var p = 0.0;

	for(var i=0; i<N; i++) {
		p += students[i].weight;
		students[i].p = p; 
	}
	students[N-1].p = 1.0;

	//for(var i=0; i<N; i++) {
	//	console.log(students[i].fname+":"+students[i].p);
	//}
	
}

function updateDisplay(student) {
	$('#displayid').text(student.id);
	$('#present').text(student.present);
	$('#absent').text(student.absent);
	$('#freq').text(student.frequency);
	$('#volunteer').text(student.volunteer);
}

function pre_plus() {
	var s = data.picked;

	s.present++;
	s.frequency = s.present + s.absent;

	updateDisplay(s);
	data.edited = true;
	saveCurrentData();
}

function pre_minus() {
	var s = data.picked;

	if (s.present > 0) {
		s.present--;
	}
	s.frequency = s.present + s.absent;

	updateDisplay(s);
	data.edited = true;
	saveCurrentData();
}

function ab_plus() {
	var s = data.picked;

	s.absent++;
	s.frequency = s.present + s.absent;

	updateDisplay(s);
	data.edited = true;
	saveCurrentData();
}

function ab_minus() {
	var s = data.picked;

	if (s.absent > 0) { 
		s.absent--;
	}
	s.frequency = s.present + s.absent;

	updateDisplay(s);
	data.edited = true;
	saveCurrentData();
}

function vo_plus() {
	var s = data.picked;

	s.volunteer++;

	updateDisplay(s);
	data.edited = true;
	saveCurrentData();
}

function vo_minus() {
	var s = data.picked;

	if (s.volunteer > 0) { 
		s.volunteer--;
	}

	updateDisplay(s);
	data.edited = true;
	saveCurrentData();
}