data = {students: null, student: null, file: null, edited: false, current: null, fn: null};
list = null;
function readFile(files) {

	//console.log("Read");

	for(var i=0, f; f = files[i]; i++) {
		data.file = f.name;
		//console.log(f.name);
		if (!f.name.match(/csv$/)) {
			alert('This application supports only CSV file.');
			break;
		}

		var reader = new FileReader();

		reader.onload = function(e) {
			//console.log(e.target.result);
			data.students = processData(e.target.result);
			data.current = data.students.slice(0);
			//console.log(data.current.length);
			if (data.students != null) {
				enableButtons();
				data.edited = false;
				saveToWebStorage();
			}
		};

		reader.onerror = (function(theFile) {
			return function(e) {
				alert("Error while reading "+theFile.name);
			};
		})(f);

		reader.readAsText(f);
	}
}

function loadList() {
	list = JSON.parse(localStorage.getItem('victimjs'));
}

function generateDropdown() {
	var d = $("<select id=\"filelist\"></select>");
	d.append($('<option selected="selected"></option>'));
	for (var i in list) {
		var t = list[i];
		var name = t.substr(2);
		d.append($('<option>').attr('value', list[i]).text(name));
	}
	$('#flist').append(d);
	d.change(loadExistingData);
}

function saveCurrentData() {
	var fn = data.fn;
	console.log("Saved "+fn);
	localStorage.setItem(fn, JSON.stringify(data));
}

function loadExistingData() {
	var slt = $('#filelist option:selected').val();
	var d = localStorage.getItem(slt);
	if (d != null) {
		//Confirm
		data = JSON.parse(d);
		data.current = data.students.slice(0);
		enableButtons();
		data.edited = false;
	}
	else {
		alert('No data found in the local storage.');
		//data = null;
	}
}

function saveToWebStorage() {
	if (Modernizr.localstorage) {
		var l = localStorage.getItem('victimjs');
		var fname = data.file;
		if (l == null) {
			//alert('No data in the localStorage');
			var fn = "1_"+fname;
			list = [fn];
			data.fn = fn;
			localStorage.setItem('victimjs', JSON.stringify(list));
			localStorage.setItem(fn, JSON.stringify(data));
		}
		else {
			//Find if fname is in the list
			list = JSON.parse(l);
			var i = list.length + 1;
			var fn = i + "_" + fname;
			list.push(fn);
			data.fn = fn;
			localStorage.setItem('victimjs', JSON.stringify(list));
			localStorage.setItem(fn, JSON.stringify(data));
		}
		return true;
	}
	else { 
		alert("Please use the browser that supports LocalStorage.");
		return false;
	}
}

function enableButtons() {
	$('#randomBtn').removeAttr('disabled');
	$('#saveBtn').removeAttr('disabled');
	$('#preplus').removeAttr('disabled');
	$('#preminus').removeAttr('disabled');
	$('#abplus').removeAttr('disabled');
	$('#abminus').removeAttr('disabled');
}

function processData(str) {
	var error = 0;
	var students = [];
	var lines = str.split("\n");
	for(var i=0; i<lines.length; i++) {
		var line = lines[i].trim();
		if (line.length == 0) {
			continue;
		}
		//console.log(i+":"+lines[i]);

		var fields = line.split(/[,\t;]/);
		if (fields.length < 1) {
			error++;
			continue;
		}
		else if (fields.length == 1) {
			var student1 =
				new Student("", fields[0], 
					"", 0, 0, 0);
		}
		else if (fields.length == 2) {
			var student2 =
				new Student(fields[0], fields[1],
					"", 0, 0, 0);
			students.push(student2);
		}
		else if (fields.length == 3) {
			var student3 = 
				new Student(fields[0], fields[1],
							fields[2], 0, 
							0, 0);
			students.push(student3);
		}
		else if (fields.length == 4) {
			var student4 = 
				new Student(fields[0], fields[1],
							fields[2], fields[3], 
							0, 0);
			students.push(student4);
		}
		else if (fields.length == 5) {
			var student5 = 
				new Student(fields[0], fields[1],
							fields[2], fields[3], 
							fields[4], 0);
			students.push(student5);
		}
		else {
			var student6 = 
				new Student(fields[0], fields[1],
							fields[2], fields[3], 
							fields[4], fields[5]);
			students.push(student6);
		}
	}
	if (error > 0) {
		alert('Error while reading the student list file');
		return null;
	}
	return students;
}

function Student(id, fname, lname, present, absent, volunteer) {
	this.id = id;
	this.fname = fname;
	this.lname = lname;
	this.present = parseInt(present);
	this.absent = parseInt(absent);
	this.frequency = parseInt(present)+parseInt(absent);
	this.volunteer = parseInt(volunteer);
}

function pad(n) {
	if (n<10) {
		return '0'+n;
	}
	else {
		return n;
	}
}

function prepareDate() {
	var d = new Date();
	return d.getFullYear() + "" + pad(d.getMonth()+1) + pad(d.getDate());
}

function saveFile() {
	var re = /\_(\d{8})\./;
	var file = data.file;
	var sfile;
	if (data.file.match(re) != null) {
		sfile = file.replace(re, "_"+prepareDate()+".");
	}
	else if (data.file.match(/\./)) {
		sfile = file.replace(".", "_"+prepareDate()+".", "i");
	}
	else {
		sfile = file + "_" + prepareDate();
	}
	var students = data.students;
	var N = students.length;

	var s = "";
	for(var i=0; i<N; i++) {
		s += students[i].id + ',';
		s += students[i].fname + ',';
		s += students[i].lname + ',';
		s += students[i].present + ',';
		s += students[i].absent + ',';
		s += students[i].volunteer + '\n';
	}

	var blob = new Blob([s], {type: "text/csv"});
	//var filesave = new FileSaver(blob, sfile);
	saveAs(blob, sfile);
	data.edited = false;
}
